<?php
declare (strict_types=1);

namespace think\admin\support\middleware;

use think\admin\service\AdminService;
use think\App;
use think\exception\HttpResponseException;
use think\Request;
use think\Response;

/**
 * 后台权限中间键
 * @class RbacAccess
 * @package think\admin\support\middleware
 */
class RbacAccess
{
    /**
     * 当前 App 对象
     * @var \think\App
     */
    protected $app;

    /**
     * Construct
     * @param \think\App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * 中间键处理
     * @param \think\Request $request
     * @param \Closure $next
     * @return \think\Response
     */
    public function handle(Request $request, \Closure $next): Response
    {
        // HTTP.LANG 语言包处理
        $langSet = $this->app->lang->getLangSet();
        if (is_file($file = dirname(__DIR__, 2) . "/lang/{$langSet}.php")) {
            $this->app->lang->load($file, $langSet);
        }

        // 动态加载全局语言包
        if (is_file($file = syspath("lang/{$langSet}.php"))) {
            $this->app->lang->load($file, $langSet);
        }

        // 跳过忽略配置应用 或 有权限访问
        $ignore = $this->app->config->get('app.rbac_ignore', []); $appName = $this->app->http->getName(); $url = $this->app->request->url();
        if (in_array($appName, $ignore)) return $next($request);

        if (in_array($appName, $this->app->config->get('app.rbac_sub'))) {
            // 分系统
            $subService = app(str_replace('sub', $appName, "\app\sub\service\SubService"));

            // 有权限访问，进入下一步
            if ($subService->check()) return $next($request);

            // 无权限已登录，提示异常
            if ($subService->isLogin()) return $request->isPost() ? throw new HttpResponseException(json(['code' => 0, 'info' => lang('禁止访问！')])) : view("{$appName}@common/noauth", ['get' => $request->get()]);

            // 无权限未登录，跳转登录
            // return $request->isPost() ? throw new HttpResponseException(json(['code' => 401, 'info' => lang('请重新登录！'), 'url' => sysuri("{$appName}/login/index")])) : view("{$appName}@common/nologin", ['get' => $request->get()]);
            $loginPage = sysuri("{$appName}/login/index");
            throw new HttpResponseException(json(['code' => 401, 'info' => lang('请重新登录！'), 'url' => "{$loginPage}#{$url}"]));
        }
        else {
            // 总管理
            // 有权限访问，进入下一步
            if (AdminService::check()) return $next($request);

            // 无权限已登录，提示异常
            if (AdminService::isLogin()) return $request->isPost() ? throw new HttpResponseException(json(['code' => 0, 'info' => lang('禁止访问！')])) : view("admin@common/noauth", ['get' => $request->get()]);

            // 无权限未登录，跳转登录
            // if ($request->isPost()) {
            //     $loginUrl = $this->app->config->get('app.rbac_login') ?: 'admin/login/index';
            //     $loginPage = preg_match('#^(/|https?://)#', $loginUrl) ? $loginUrl : sysuri($loginUrl);
            //     throw new HttpResponseException(json(['code' => 401, 'info' => lang('请重新登录！'), 'url' => $loginPage]));
            // }
            // else return view("admin@common/nologin", ['get' => $request->get()]);
            $loginUrl = $this->app->config->get('app.rbac_login') ?: 'admin/login/index';
            $loginPage = preg_match('#^(/|https?://)#', $loginUrl) ? $loginUrl : sysuri($loginUrl);
            throw new HttpResponseException(json(['code' => 401, 'info' => lang('请重新登录！'), 'url' => "{$loginPage}#{$url}"]));
        }
    }
}