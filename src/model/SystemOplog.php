<?php
declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 系统日志模型
 * @class SystemOplog
 * @package think\admin\model
 */
class SystemOplog extends Model
{
    /**
     * 格式化创建时间
     * @param mixed $value
     * @return string
     */
    public function getCreateAtAttr($value): string
    {
        return format_datetime($value);
    }

    /**
     * 格式化操作数据
     * @param string $value
     * @return string
     */
    public function getDataAttr($value)
    {
        return is_string($value) ? json_decode($value, true) : [];
    }
}